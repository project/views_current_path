<?php

namespace Drupal\views_current_path\Plugin\views\field;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\PreviewFallbackInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;

/**
 * Default implementation of the base field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("current_path")
 */
class CurrentPath extends FieldPluginBase implements PreviewFallbackInterface {

  /**
   * Implement query behaviour.
   *
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   *
   * @return array
   *   An array of the available options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['path_format'] = ['default' => 'raw-internal'];
    $options['query_string_support'] = ['default' => 'bypass-query-string'];
    $options['query_params_filter'] = ['default' => []];
    $options['query_params_filter_with_value'] = ['default' => []];
    $options['query_params_renaming'] = ['default' => []];
    $options['query_params_trim'] = ['default' => FALSE];
    $options['query_params_lower'] = ['default' => FALSE];
    $options['cache_user'] = ['default' => FALSE];

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $moduleHandler = \Drupal::service('module_handler');
    $alias_required_message = $moduleHandler->moduleExists('path') ? '' : '<em>'
      . t('Note: the Path module must be enabled for the "Alias" options to work.') . '</em>';

    // Determine the URL prefix.
    $base_url = $GLOBALS['base_url'];
    global $base_path;

    $url_options = [];
    $raw_relative_prefix = $base_path . ($url_options['prefix'] ?? '');
    $raw_absolute_prefix = $base_url . $raw_relative_prefix;

    $raw_example = 'node/215';
    $alias_example = 'pages/example-path';
    $query_string_example = '?nid=357&tid=271';
    $query_string_valid_path_example = '[current_path]tid=[tid]';
    $query_string_invalid_path_example = '[current_path]?tid=[tid]';

    $form['path_format'] = [
      '#type' => 'radios',
      '#title' => t('Output style'),
      '#description' => $alias_required_message,
      '#options' => [
        'raw-internal' => t('Raw internal path (e.g. @example)', ['@example' => $raw_example]),
        'raw-relative' => t('Raw relative URL (e.g. @example)', ['@example' => $raw_relative_prefix . $raw_example]),
        'raw-absolute' => t('Raw absolute URL (e.g. @example)', ['@example' => $raw_absolute_prefix . $raw_example]),
        'alias-internal' => t('Alias internal path (e.g. @example)', ['@example' => $alias_example]),
        'alias-relative' => t('Alias relative URL (e.g. @example)', ['@example' => $raw_relative_prefix . $alias_example]),
        'alias-absolute' => t('Alias absolute URL (e.g. @example)', ['@example' => $raw_absolute_prefix . $alias_example]),
        'query-only' => t('Query string only (e.g. @example)', ['@example' => $query_string_example]),
      ],
      '#default_value' => $this->options['path_format'],
    ];

    $qs_message = t('<p>Query strings are included in the alias relative URL output style.</p>');
    $qs_message .= t('<p>Use query string support when creating a views path link rewrite and a query string is to be included.</p>');
    $qs_message .= t('For example, if building a link such as "@example" with multiple key-value pairs, the "concatenate" option would be required.',
      ['@example' => $raw_relative_prefix . $alias_example . $query_string_example]);
    $qs_message .= t('</p><p><em>Note: If using this feature, do not include a question mark (?) in the Views path link rewrite. A proper rewrite for query string support would be "@example_valid", and not "@example_invalid".',
      [
        '@example_valid' => $query_string_valid_path_example,
        '@example_invalid' => $query_string_invalid_path_example,
      ]);
    $qs_message .= t('A question mark will be added by the module if necessary.</em></p>');
    $form['query_string_support'] = [
      '#type' => 'radios',
      '#title' => t('Query string handling for alias relative URL'),
      '#options' => [
        'bypass-query-string' => t("Don't modify the query string"),
        'remove-query-string' => t('Remove existing query string on the current path'),
        'replace-query-string' => t('Replace existing query string on the current path with values passed through path rewrite'),
        'concat-query-string' => t('Concatenate existing query string on the current path with values passed through the path rewrite'),
      ],
      '#description' => $qs_message,
      '#default_value' => $this->options['query_string_support'],
      '#states' => [
        'visible' => [':input[name="options[path_format]"]' => ['value' => 'alias-relative']],
      ],
    ];

    foreach ($this->options['query_params_filter'] as $key => $param) {
      if (in_array($param, $this->options['query_params_filter_with_value'])) {
        $this->options['query_params_filter'][$key] .= '=';
      }
    }
    $form['query_params_filter'] = [
      '#type' => 'textarea',
      '#title' => t('Filter query parameters'),
      '#description' => $this->t('Only keep the query parameters listed here, one parameter per line. An empty list means no filtering. Append "=" to the parameter name to only keep the parameter if it has a value.'),
      '#default_value' => implode("\n", $this->options['query_params_filter']),
      '#states' => [
        'visible' => [':input[name="options[path_format]"]' => ['value' => 'query-only']],
      ],
    ];
    $form['query_params_renaming'] = [
      '#type' => 'textarea',
      '#title' => t('Query parameters renaming'),
      '#description' => $this->t('Rename query parameters listed here, one parameter per line, seperated by "|". Format: original_name|new_name.'),
      '#default_value' => implode("\n", $this->options['query_params_renaming']),
      '#states' => [
        'visible' => [':input[name="options[path_format]"]' => ['value' => 'query-only']],
      ],
    ];
    $form['query_params_trim'] = [
      '#type' => 'checkbox',
      '#title' => t('Remove trailing spaces'),
      '#description' => $this->t('Remove trailing spaces from query parameter values.'),
      '#default_value' => $this->options['query_params_trim'],
      '#states' => [
        'visible' => [':input[name="options[path_format]"]' => ['value' => 'query-only']],
      ],
    ];
    $form['query_params_lower'] = [
      '#type' => 'checkbox',
      '#title' => t('Convert to lower case'),
      '#description' => $this->t('Convert query parameter values to lower case.'),
      '#default_value' => $this->options['query_params_lower'],
      '#states' => [
        'visible' => [':input[name="options[path_format]"]' => ['value' => 'query-only']],
      ],
    ];

    $form['view_edit_notice'] = [
      '#markup' => '<p>Note: ' . t('The placeholder @placeholder will be used for the field value while editing the view.',
        ['@placeholder' => '[' . $this->options['id'] . ']']) . '</p>',
      '#default_value' => '',
    ];

    $caching_message_user = '<p>' . t('Enable user-based caching to cache this view based on the currently logged in user. This will rebuild the view for each user') . '</p>';

    $form['cache_user'] = [
      '#type' => 'checkbox',
      '#title' => t('Use user-based caching'),
      '#description' => $caching_message_user,
      '#default_value' => $this->options['cache_user'],
    ];

    parent::buildOptionsForm($form, $form_state);

  }

  /**
   * Submit handler for the options form.
   *
   * @{inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    $options = &$form_state->getValue('options');
    $options['query_params_filter'] = array_filter(array_map('trim', preg_split('/(\r\n?|\n)/',  $options['query_params_filter'])));
    $options['query_params_filter_with_value'] = [];
    foreach ($options['query_params_filter'] as $key => $param) {
      if (preg_match('/(\w+)=$/', $param, $matches)) {
        $options['query_params_filter_with_value'][] =
          $options['query_params_filter'][$key] = $matches[1];
      }
    }
    $options['query_params_renaming'] = array_filter(array_map('trim', preg_split('/(\r\n?|\n)/',  $options['query_params_renaming'])));
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($this->view->preview) {
      return $this->getPreviewFallbackString();
    }

    $build = ['#markup' => $this->getValue($values)];
    $metadata = new CacheableMetadata();

    if ($this->options['path_format'] === 'query-only' && $this->options['query_params_filter']) {
      foreach($this->options['query_params_filter'] as $param) {
        $metadata->addCacheContexts(['url.query_args:' . $param]);
      }
    }
    else {
      $metadata->addCacheContexts(['url.query_args']);
    }

    if ($this->options['cache_user']) {
      $metadata->addCacheContexts(['user']);
    }

    $metadata->applyTo($build);

    return $build;
  }

  /**
   * @{inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    $current_path = \Drupal::service('path.current')->getPath();

    // Determine the URL prefix.
    global $base_url, $base_path;
    $url_options = [];
    $raw_relative_prefix = $base_path . ($url_options['prefix'] ?? '');
    $raw_absolute_prefix = $base_url . $raw_relative_prefix;

    // Determine the path.
    switch ($this->options['path_format']) {
      case 'raw-relative':
        $output = $raw_relative_prefix . $current_path;
        break;

      case 'raw-absolute':
        $output = $raw_absolute_prefix . $current_path;
        break;

      case 'alias-internal':
        $output = \Drupal::request()->getRequestUri();
        break;

      case 'alias-relative':
        $output = \Drupal::request()->getRequestUri();
        // If using alias-relative, process query string support setting.
        switch ($this->options['query_string_support']) {
          // If bypass is selected, skip any changes.
          case 'bypass-query-string':
            break;

          case 'remove-query-string':
            if (stripos($output, '?') !== FALSE) {
              $output = strtok($output, '?');
            }
            break;

          case 'replace-query-string':
            if (stripos($output, '?') !== FALSE) {
              $output = strtok($output, '?') . '?';
            }
            break;

          case 'concat-query-string':
            if (stripos($output, '?') !== FALSE) {
              $output .= '&';
            }
            else {
              $output .= '?';
            }
            break;

          default:
            // Just as if bypass is selected -- skip any changes.
            break;

        }
        break;

      case 'alias-absolute':
        $option = [
          'absolute' => TRUE,
        ];
        $output = Url::fromUri('internal:' . $current_path, $option)->toString();
        break;

      case 'query-only':
        $q_items = [];
        parse_str($_SERVER["QUERY_STRING"], $q_items);
        // Don't include useless "q=" that some servers return.
        unset($q_items['q']);

        if ($this->options['query_params_filter']) {
          foreach(array_keys($q_items) as $param) {
            if (
              !in_array($param, $this->options['query_params_filter']) ||
              (in_array($param, $this->options['query_params_filter_with_value']) && empty($q_items[$param]) && $q_items[$param] !== '0')
            ) {
              unset($q_items[$param]);
            }
            else {
              if ($this->options['query_params_trim']) {
                $q_items[$param] = trim($q_items[$param]);
              }
              if ($this->options['query_params_lower']) {
                $q_items[$param] = mb_strtolower($q_items[$param]);
              }
              foreach ($this->options['query_params_renaming'] as $renaming) {
                if (preg_match('/^' . preg_quote($param, '/'). '\|([^|]+)$/', $renaming, $matches)) {
                  $q_items[$matches[1]] = $q_items[$param];
                  unset($q_items[$param]);
                }
              }
            }
          }
        }

        $output = UrlHelper::buildQuery($q_items);

        break;

      case 'raw-internal':
      default:
        $output = $current_path;

        break;
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviewFallbackString() {
    return $this->t('Placeholder for global current path field @id.', ['@id' => $this->options['id']]);
  }

}
